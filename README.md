# DEFESTA GUIDE

배포현황: [![Netlify Status](https://api.netlify.com/api/v1/badges/0929872c-924b-4a51-81ea-2af6263e17ae/deploy-status)](https://app.netlify.com/sites/dfesta-22224444/deploys)

디페스타는 netlify[https://app.netlify.com/sites/dfesta-22224444] 웹호스팅 서비스를 이용합니다.  
테스트, 라이브 반영 방법 최하단 가이드 참조

## 디렉토리 구조

```bash
├── assets
│   ├── fonts - 폰트
│   ├── img - 이미지
│   │   └── data - 데이터 이미지
│   ├── scss - 스타일시트
│   └── video - main.mp4 history페이지에서만 로컬 영상으로 쓰임
├── components
│   └── ... 컴포넌트 목록
├── data - 데이터
│   ├── artistData.js - 아티스트 페이지 관련 데이터
│   ├── goodsData.js - Goods 페이지 전용 데이터
│   ├── routeData.js - GNB 컴포넌트 라우트 설정
│   └── spotlightData.js - 스포트라이트 페이지 관련  데이터
├── layout
│   └── ... 레이아웃 디폴트 설정
├── pages
│   └── ... 페이지
├── plugins
│   ├── directive.js - 공통 기능 (기능 디렉티브 선언)
│   ├── mixins.js - 공통 기능 (UI 기능)
│   ├── plugin.js - 공통 프레임워크 등록, import
│   └── vue-awesome-swiper.js - 스와이퍼 프레임워크 등록, import
├── static
│   └── ... 사이트 정보 이미지
├── nuxt.config.js - 사이트 메타정보포함 글로벌 설정, 개발옵션 등등 공통 구성
└── .npmrc - GSAP 프레임워크 라이센스 키등록
```

## 브랜치 전략

- main - https://develop.dfesta.co.kr/ (라이브)
- develop - https://develop.dfesta.co.kr/ (테스트)
- site - 작업브랜치

## 데이터 전략

`data/artistData.js`

1. index - 아티스트 섹션에 썸네일, 이름 활용
2. artist/ - 아티스트 목록페이지에 썸네일, 이름 활용
3. artist/${detail} - 해당 아티스트에 대한 디테일 페이지에 상세정보 전체 데이터 사용

`data/spotlightData.js`

1. index - spotlight 영역 마지막 포스팅 기준 정보 제공
2. GNB - 마지막 포스팅 기준 하단 썸네일, 타이틀 제공
3. aritst/${detail} - SPOTLIGHT D’FESTA 영역 해당 아티스트의 정보 제공
4. spotlight/ - 최근 업데이트 순으로 목록 데이터 제공
5. spotlight/${detail} - 피드 선택한 아티스트의 뉴스, 상세 정보 제공

## Spotlight Data Guide

```js
{
    name: 'enhypen', // 판별용 아티스트 데이터 이름
    artistName: 'enhypen', // 출력용 아티스트 이름
    nowPost: [ // 포스팅 데이터
      {
        id: '00025', // 포스팅 ID 업데이트시 증감함
        thumb: require('@/assets/img/data/spot_thumbnail_b_enhypen_25.jpg'), // 목록형에 게시할 썸네일
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_enhypen_25.jpg'), // 메인에 게시할 썸네일
        title: '엔하이픈이 빛나는 비주얼을 뽐냈다', // 타이틀
        text: `지난 9일 오후 서울 영등포구 여의도동 IFC몰에서 ‘디페스타’ VIP시사회 행사가 진행됐다....`, // 내용
        nowDetails: { // 스포트라이트 디테일에 표현할 데이터
          coverlImage: require('@/assets/img/data/top_enhypen.jpg'), // 상단 커버
          moCoverlImage: require('@/assets/img/data/top_enhypen_m.jpg'), // 모바일용 상단 커버
          detailCover: require('@/assets/img/data/spot_cover_enhypen_25.jpg'), // 상단 메인 이미지
          videoOptions: { // Youtube 영상
            id: '-lrxsli3V88', // Youtube id
          },
          puzzleList: [ // 2단 컬럼형으로 된 영역(max 3개)
            {
              thumb: [
                require('@/assets/img/data/00025_puzzle_01.jpg'), // 정비율 이미지
                require('@/assets/img/data/00025_puzzle_02.jpg'), // 세로 큰이미지
                require('@/assets/img/data/00025_puzzle_03.jpg'), // 정비율 이미지
              ],
            },
          ],
          gallery: { // 슬라이드형 이미지
            thumb: require('@/assets/img/data/00025_still_01.jpg')
          },
          stillList: [ // 스틸컷 이미지
            {
              thumb: require('@/assets/img/data/00025_still_01.jpg'),
            },
            {
              thumb: require('@/assets/img/data/00025_still_02.jpg'),
            },
            {
              thumb: require('@/assets/img/data/00025_still_03.jpg'),
            },
            {
              thumb: require('@/assets/img/data/00025_still_04.jpg'),
            },
            {
              thumb: require('@/assets/img/data/00025_still_05.jpg'),
            },
            {
              thumb: require('@/assets/img/data/00025_still_06.jpg'),
            },
            {
              thumb: require('@/assets/img/data/00025_still_07.jpg'),
            },
          ],
        },
      },
      ...
    ],
  },
```

## Develop Start

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build

# generate static project
$ npm run generate
```

## netlify 테스트서버, 라이브 배포 방법

> git push만 하면 서버에 반영이 되는 구조입니다.

### develop

```bash
1. git merge ${feature barnch}
2. git push origin develop
```

### main

```bash
1. git merge ${feature barnch}
2. git push origin main
```
