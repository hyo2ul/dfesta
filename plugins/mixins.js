const mixins = {
  data() {
    return {
      isMobile: false,
      isScrollTop: false,
      lastScrollTop: 0,
    }
  },
  beforeDestroy() {
    if (typeof window === 'undefined') return
    window.removeEventListener('resize', this.onResize)
    window.removeEventListener('scroll', this.onScroll)
  },
  mounted() {
    this.onResize()
    this.onScroll()
    window.addEventListener('resize', this.onResize)
    window.addEventListener('scroll', this.onScroll)
  },
  methods: {
    onResize() {
      const html = document.documentElement
      const windowWidth = window.innerWidth
      const contentHeight = document.querySelector('#smooth-content').clientHeight
      this.isMobile = window.innerWidth < 1025
      html.style.setProperty('--windowWidth', `${windowWidth}px`)
      html.style.setProperty('--contentHeight', `${contentHeight}px`)
    },
    onScroll(evt) {
      const html = document.documentElement
      const scrollY = window.scrollY
      const st = scrollY || document.documentElement.scrollTop
      html.style.setProperty('--scrollY', `${scrollY}px`)
      if (st > window.innerHeight) {
        if (st > this.lastScrollTop) {
          // downscroll code
          this.isScrollTop = true
        } else {
          this.isScrollTop = false
        }
      }

      this.lastScrollTop = st <= 0 ? 0 : st
    },
  },
}

export default mixins
