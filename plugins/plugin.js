import Vue from 'vue'
import anime from 'animejs/lib/anime.es.js'
import axios from 'axios'
import VScrollLock from 'v-scroll-lock/dist/v-scroll-lock-no-dep.esm'
import { gsap } from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger.js'
import ScrollSmoother from 'gsap/ScrollSmoother.js'
import SplitText from 'gsap/SplitText.js'
import { enableBodyScroll, disableBodyScroll } from 'body-scroll-lock'

gsap.registerPlugin(ScrollTrigger, ScrollSmoother, SplitText)
// use
Vue.use(VScrollLock, {
  enableBodyScroll,
  disableBodyScroll,
})

Vue.prototype.$anime = anime
Vue.prototype.$axios = axios
Vue.prototype.$gsap = gsap
Vue.prototype.$ScrollTrigger = ScrollTrigger
Vue.prototype.$ScrollSmoother = ScrollSmoother
Vue.prototype.$SplitText = SplitText
Vue.prototype.$enableBodyScroll = enableBodyScroll
Vue.prototype.$disableBodyScroll = disableBodyScroll
export default (context, inject) => {
  inject('gsap', gsap)
}

// export default ({ app, store }) => {
//   // Set i18n instance on app
//   // This way we can use it in middleware and pages asyncData/fetch
//   app.i18n = new VueI18n({
//     locale: store.state.locale,
//     // fallbackLocale: 'en',
//     // messages: {
//     //   en: require('~/locales/en.json'),
//     //   fr: require('~/locales/fr.json'),
//     // },
//   })
//   app.i18n.path = (link) => {
//     if (app.i18n.locale === app.i18n.fallbackLocale) {
//       return `/${link}`
//     }
//     return `/${app.i18n.locale}/${link}`
//   }
// }
