const routeData = [
  {
    title: 'INTRODUCE',
    submenu: [
      {
        name: 'd’festa',
        path: '/introduce',
      },
      {
        name: 'the movie',
        path: '/introduce/the-movie',
      },
      {
        name: 'the exhibition',
        path: '/introduce/the-exhibition',
      },
      {
        name: 'the experience',
        path: '/introduce/the-experience',
      },
    ],
  },
  {
    title: 'artist',
    submenu: [
      {
        name: 'enhypen',
        path: '/artist/enhypen',
      },
      {
        name: 'Stray Kids',
        path: '/artist/straykids',
      },
      {
        name: 'NCT 127',
        path: '/artist/nct127',
      },
      {
        name: 'SEVENTEEN',
        path: '/artist/seventeen',
      },
      {
        name: 'nu’est',
        path: '/artist/nuest',
      },
      {
        name: 'txt',
        path: '/artist/txt',
      },
      {
        name: 'nct dream',
        path: '/artist/nctdream',
      },
      {
        name: 'TWICE',
        path: '/artist/twice',
      },
      {
        name: 'BTS ',
        path: '/artist/bts',
      },
    ],
    moSubmenu: [
      {
        name: 'enhypen',
        path: '/artist/enhypen',
      },
      {
        name: 'txt',
        path: '/artist/txt',
      },
      {
        name: 'Stray Kids',
        path: '/artist/straykids',
      },
      {
        name: 'nct dream',
        path: '/artist/nctdream',
      },
      {
        name: 'NCT 127',
        path: '/artist/nct127',
      },
      {
        name: 'TWICE',
        path: '/artist/twice',
      },
      {
        name: 'SEVENTEEN',
        path: '/artist/seventeen',
      },
      {
        name: 'BTS ',
        path: '/artist/bts',
      },
      {
        name: 'nu’est',
        path: '/artist/nuest',
      },
    ],
  },
  {
    title: 'spotlight',
    submenu: [], // 포스팅 마지막 데이터 추가
  },
  {
    title: 'now arrival',
    path: '/introduce/now-arrival',
  },
  {
    title: 'history',
    submenu: [
      {
        name: 'D’FESTA TOKYO I',
        path: '/introduce/history-tokyo',
      },
      {
        name: 'D’FESTA SEOUL',
        path: '/introduce/history-seoul',
      },
    ],
  },
  {
    title: 'GOODS',
    path: '/introduce/goods',
  },
]

export default routeData
