const nowData = [
  {
    name: 'enhypen',
    artistName: 'enhypen',
    nowPost: [
      {
        id: '00063',
        thumb: require('@/assets/img/data/spot_thumbnail_b_enhypen_63.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_enhypen_63.jpg'),
        title: `아름답고 황홀하게 <br> D’FESTA에 방문한 <br> ‘ENHYPEN’`,
        text: `지난 5월 9일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’에 ‘엔하이픈’ 정원, 희승, 제이, 제이크, 성훈, 선우, 니키가 방문하였다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_enhypen.jpg'),
          moCoverlImage: require('@/assets/img/data/top_enhypen_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_enhypen_63.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772326774',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  thumb: require('@/assets/img/data/00063_still_01.jpg'),
                  caption:
                    '디페스타 행사에 참석하기 전, 모든 아티스트의 필수 관문인 대형 사인월에서 즐거운 포토타임을 가졌다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00063_still_02.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00063_still_03.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00063_still_04.jpg'),
                    },
                  ],
                  caption:
                    '다음으로 LED 배경이 그대로 나오는 ‘THE EXPERIENCE’존에 입장했다. 인증샷과 챌린지 영상까지 야무지게 찍으며 즐겁게 즐기는 엔하이픈이었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00063_still_05.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00063_still_06.jpg'),
                  caption:
                    '팝콘까지 야무지게 먹으며 디페스타의 하이라이트 더무비를 감상중인 엔하이픈. 웅장한 인트로를 지나 XR 무대가 나오자 여기저기서 감탄사가 나왔다. 멤버들 모두 촬영 당시를 떠올리며 영상에 푹 빠진 모습이었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00063_still_07.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00063_still_08.jpg'),
                  caption:
                    '디페스타 두번째 공간 THE EXHIBITION 공간에 도착하였다. 서로의 사진을 찾으며 메시지를 남기느라 바쁜 엔하이픈의 모습에 전시를 즐긴 관람객들도 덩달아 즐거웠던 현장이었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00063_still_09.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00063_still_10.jpg'),
                  caption:
                    '데뷔한지 오래되진 않았지만 그동안의 추억을 새록새록 떠올리며 함께 나누는 엔하이픈',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00063_still_11.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00063_still_12.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00063_still_13.jpg'),
                    },
                  ],
                  caption:
                    '또다른 화보 사진이 있는 팀별 콘셉트 화보로 구성되어 있는 스키즈존에 도착하였다. 감상은 잠시 뒤로 하고, 디페스타를 방문하는 스테이를 위한 애정 가득한 메모와 사인을 남겼다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00063_still_14.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00063_still_15.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00063_still_16.jpg'),
                    },
                  ],
                  caption:
                    '멤버들이 직접그린 캔버스 제작과정 영상 먼저 본 후 함께 참여하여 만든 작업물 앞에서 인증샷도 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00063_still_17.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00063_still_18.jpg'),
                  caption:
                    '굿즈 쇼핑을 마친 엔하이픈은 (디페스타에) 굉장히 많은 선배님들이 계셨는데 저희가 여기 있음에 영광스럽고 다음에도 기회가 되면 함께하고 싶다는 마지막 인사말을 남기며 행사를 마무리했다.',
                },
              ],
            },
          ],
        },
      },
    ],
  },
  {
    name: 'txt',
    artistName: 'txt',
    nowPost: [
      {
        id: '00062',
        thumb: require('@/assets/img/data/spot_thumbnail_b_txt_62.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_txt_62.jpg'),
        title: `별을 쫓아 <br /> D’FESTA를 찾은 <br> ‘투모로우바이투게더’`,
        text: `지난 4월 11일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’의 VIP 시사회가 진행됐다. 이날 행사에는 ‘투모로우바이투게더’가 참석해 영화와 사진전을 관람하며, 디페스타를 즐기는 모습을 보여주었다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_txt.jpg'),
          moCoverlImage: require('@/assets/img/data/top_txt_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_txt_62.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772328816',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  thumb: require('@/assets/img/data/00062_still_01.jpg'),
                  caption:
                    '본격적인 디페스타 즐기기에 앞서 아티스트들의 친필 사인이 있는 대형 ‘Sign Wall’에서 포토타임을 가졌다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00062_still_02.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00062_still_03.jpg'),
                  caption:
                    '첫 번째 관람 순서인 ‘THE MOVIE’로 향하는 멤버들은 영화관 나들이가 오랜만인 듯 설렘을 감추지 못했다. 독특한 스토리로 시작하는 영화에 집중하는 듯했던 멤버들은 이내 스크린을 가득 채운 멤버들 얼굴을 보고 서로 빵 터져버렸다. 하지만 그것도 잠시, 엄청난 스케일의 영상과 음향 덕에 영화에 푹 빠져든 모습이었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00062_still_04.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00062_still_05.jpg'),
                  caption:
                    '영화의 감동을 뒤로 하고 영화관에서 자연스럽게 이어지는 동선을 따라 화보를 보기 위해 ‘THE EXHIBITION’에 도착했다. 각자 멤버들과 자신의 사진에 메시지를 남기며 즐거운 모습이었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00062_still_06.jpg'),
                  caption:
                    '‘MEMORY ROOM’으로 입장한 멤버들은 데뷔 초 사진을 보며 추억 여행에 빠진 듯 했다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00062_still_07.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00062_still_08.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00062_still_09.jpg'),
                    },
                  ],
                  caption:
                    '메모리룸에서 나와 메인 화보를 감상한 투바투 멤버들은 전시 관람하러 올 모아들을 위해 정성껏 사인을 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00062_still_10.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00062_still_11.jpg'),
                  caption:
                    '모아들을 위한 사인을 남긴 멤버들은 굿즈샵에서 모아로 완벽히 빙의한 모습이었다. 직접 그린 작품이 담긴 가방을 받은 멤버들은 색감이 너무 예쁘다며 어린아이처럼 좋아했다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00062_still_12.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00062_still_13.jpg'),
                  caption:
                    '마지막 관람 순서인 ‘THE EXPERIENCE’에 입장한 멤버들은 모아처럼 인증샷도 남기고, 챌린지 영상까지 야무지게 촬영하며 이날 행사를 마무리했다. 연준은 “너무 다채롭고 볼거리가 많아 케이팝을 사랑하는 팬분들께서 너무 좋아하지 않을까 싶습니다”라며 센스있는 마지막 인사까지 남겼다.',
                },
              ],
            },
          ],
        },
      },
    ],
  },
  {
    name: 'straykids',
    artistName: 'Stray Kids',
    nowPost: [
      {
        id: '00064',
        thumb: require('@/assets/img/data/spot_thumbnail_b_straykids_64.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_straykids_64.jpg'),
        title: `어서 오십시오~! <br> D’FESTA를 찾은 <br> 치명적인 매력의 <br>‘STRAY KIDS’`,
        text: `지난 5월 11일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’에 시끌벅적 소리꾼 ‘스트레이키즈’ 한, 필릭스, 리노, 승민이 방문하였다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_straykids.jpg'),
          moCoverlImage: require('@/assets/img/data/top_straykids_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_straykids_64.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772329438',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  thumb: require('@/assets/img/data/00064_still_01.jpg'),
                  caption:
                    '본격적인 디페스타 관람에 앞서 69명 아티스트 친필 사인이 있는 대형 사인월에서 포토타임을 가지며 멋진 포즈를 취했다. 수많은 스테이와 카메라 앞이라 잔뜩 긴장한 것도 잠시, 프로답게 서로 하트 포즈를 취하며 팬들의 마음을 훔쳤다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00064_still_02.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00064_still_03.jpg'),
                  caption:
                    '다음으로 신메뉴 무대의 LED 배경이 그대로 나오는 ‘THE EXPERIENCE’존으로 향했다. 인증샷도 남기고 현장감도 제대로 느끼며 잔뜩 신나는 분위기에서 함께 신메뉴 댄스 챌린지까지 알차게 즐겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00064_still_04.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00064_still_05.jpg'),
                  caption:
                    '편안한 의자에 몸을 맡겨 디페스타의 하이라이트 더무비를 감상중인 네 남자. 웅장한 인트로를 지나 LED 무대가 나오자 동시에 응원법을 외치며 감탄했다. 멤버들은 모두 촬영 당시를 떠올리며 영상에 빠져들었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00064_still_06.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00064_still_07.jpg'),
                  caption:
                    '다음 코스를 위한 레드카펫을 지나 두번째 공간 THE EXHIBITION 공간에 도착했다. 입장하자마자 바로 멤버들의 사진에 코멘트를 남기는 스트레이키즈. 첫 메시지의 주인공은 오늘 함께 참석하지 못한 리더 ‘방찬’이었다. 한은 바로 짓궂은 나이공격 멘트를 남기며 즐거워했다. 서로의 사진을 찾아다니며 개구진 모습을 보여줬다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00064_still_08.jpg'),
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00064_still_09.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00064_still_10.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00064_still_11.jpg'),
                    },
                  ],
                  caption:
                    '마지막을 장식한 승민의 사진에 코멘트와 사인을 모두 마치고, 이어서 진행되는 개인사진 촬영 타임을 가졌다. 모두 본인의 흑백 사진 앞에서 한껏 멋진 포즈를 취하였다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00064_still_12.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00064_still_13.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00064_still_14.jpg'),
                    },
                  ],
                  caption:
                    '데뷔 초부터 지금까지의 추억이 담겨있는 판도라 상자 MEMORY ROOM으로 발걸음을 옮긴 멤버들. 걱정 반 기대 반 속 당당히 입장하였다. 한은 “촬영 전날 라면을 먹고 자서 볼 빵빵한 사진 있을텐데”라며 웃음기를 머금고 촬영 일화를 털어놓았다. 과연 어떤 사진들이 스키즈를 깜짝 놀라게 만들까? 9팀의 수많은 아티스트 중 스키즈 찾는 재미를 즐기며 한참을 추억 여행하는 시간을 가졌다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00064_still_15.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00064_still_16.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00064_still_17.jpg'),
                    },
                  ],
                  caption:
                    '또다른 화보 사진이 있는 팀별 콘셉트 화보로 구성되어 있는 스키즈존에 도착하였다. 감상은 잠시 뒤로 하고, 디페스타를 방문하는 스테이를 위한 애정 가득한 메모와 사인을 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00064_still_18.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00064_still_19.jpg'),
                  caption:
                    '다음 장소는 9팀이 직접 그린 대형 캔버스 작품이 전시된 공간이다. 제작 과정이 담긴 영상 먼저 보기로! 첫번째 순서는 고뇌중인 리더 ‘방찬’의 뒤태. 리노가 오~ 나 생각보다 잘했네?”라고 말하자 한은 “선 두개 그었는데요?” 라며 개구지게 장난치며 시간을 보냈다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00064_still_20.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00064_still_21.jpg'),
                  caption:
                    '앞서 관람한 대형 캔버스 예술작품 앞에서 인증사진을 찰칵! 직접 쇼핑한 디페스타의 굿즈들로 가득 채운 에코백을 들고 찍으니 더 신나는 ‘스트레이키즈’이었다. 알차게 굿즈 쇼핑도 마친 스트레이키즈는 ‘지금까지 밟아온 과정을 다시 볼 수 있는 추억을 만들어줘서 감사하다는 마지막 인사말을 남기며 행사를 마무리했다.',
                },
              ],
            },
          ],
        },
      },
      {
        id: '00065',
        thumb: require('@/assets/img/data/spot_thumbnail_b_straykids_65.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_straykids_65.jpg'),
        title: `BANG BANG <br> BANG BOOM <br> 식지 않는 열기의 <br> ‘STRAY KIDS’`,
        text: `지난 5월 12일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’에 ‘스트레이키즈’의 현진, 아이엔, 방찬, 창빈이 관람을 위해 방문하였다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_straykids.jpg'),
          moCoverlImage: require('@/assets/img/data/top_straykids_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_straykids_65.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772329438',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  thumb: require('@/assets/img/data/00065_still_01.jpg'),
                  caption:
                    '9팀의 아티스트 친필 사인이 있는 디페스타 공통코스 SIGN WALL에서 포토타임을 가지며 멋진 포즈를 취했다. 리노와 현진은 서로의 목걸이를 뽑으며 운명의 데스티니라고 웃고 떠들었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00065_still_02.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00065_still_03.jpg'),
                  caption:
                    '다음은 스트레이키즈의 디페스타 무대 중 하나인 신메뉴 라이브와 LED 배경이 나오는 ‘THE EXPERIENCE’존으로 향했다. 거울 셀카 맛집 체험존인 만큼 서로 옹기종기 모여서 사진도 찍고, 각자 개성 넘치는 셀카도 남겼다. 셀카 타임에 이어 신나게 신메뉴 댄스 챌린지까지 야무지게 즐긴 멤버들.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00065_still_04.jpg'),
                  caption:
                    '디페스타 하면 빼놓을 수 없는 ‘THE MOVIE’로 향하는 멤버들. 가는 길 곳곳에 디페스타 흔적이 가득해 상영관과 가까워질수록 커지는 기대감을 감추지 못했다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00065_still_05.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00065_still_06.jpg'),
                  caption:
                    '의자에 착석하여 본격적인 영상 시작 전 모두 디페스타 촬영시를 떠올렸다. 창빈은 촬영 현장에서도 멤버들이 신기해했던 무대라며 추억을 회상하며 멤버들과 그때의 추억을 나누었다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00065_still_07.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00065_still_08.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00065_still_09.jpg'),
                    },
                  ],
                  caption:
                    '영화관과 연결되어 있는 화려한 레드카펫을 지나 다음 장소로 향하여 9팀의 아티스트 사진이 한곳에 모인 ‘THE EXHIBITION’존에 도착하였다. 한은 어제 먼저 방문한 멤버들이 남긴 나이공격 코멘트를 보며 웃음을 참지 못했다. 각자의 사진에 메시지를 남긴 멤버들을 맞추느라 바쁜 모두였다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00065_still_10.jpg'),
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00065_still_11.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00065_still_12.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00065_still_13.jpg'),
                    },
                  ],
                  caption:
                    '추억 가득한 MEMORY ROOM에서 또 하나의 즐거운 추억을 쌓는 멤버들. 변함없이 여전히 깜찍한 막내 아이엔의 귀여움 앞에 모두가 무너졌다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00065_still_14.jpg'),
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00065_still_15.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00065_still_16.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00065_still_17.jpg'),
                    },
                  ],
                  caption:
                    '약간의 대기 후 단체사진 존으로 들어가자 펼쳐진 9팀의 아티스트 사진을 보며 창빈은 여기 걸려있는게 영광이라는 진심 어린 감회를 내비쳤다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00065_still_18.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00065_still_19.jpg'),
                  caption:
                    'THE EXHIBITION 존의 마지막 9팀의 대형 캔버스 작품이 전시된 공간으로 이동하였다. 8명이 함께 만들어낸 만큼 더욱 만족스럽고 의미 있는 그림이었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00065_still_20.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00065_still_21.jpg'),
                  caption:
                    '마지막까지 굿즈 쇼핑도 알차게 즐기는 멤버들! 다양한 굿즈들을 가득 담은 에코백을 들고 작품 앞에서 한컷을 찍으며 행사를 마무리했다.',
                },
              ],
            },
          ],
        },
      },
    ],
  },
  {
    name: 'nctdream',
    artistName: 'nct dream',
    nowPost: [
      {
        id: '00066',
        thumb: require('@/assets/img/data/spot_thumbnail_b_dream_66.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_dream_66.jpg'),
        title: `드리미들도 푹빠진 <br> 디페스타의 현장속으로 <br> ‘NCT DREAM’`,
        text: `지난 4월 22일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’에 NCT DREAM의 천러, 해찬, 마크가 디페스타 관람을 위해 발걸음 하였다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_dream.jpg'),
          moCoverlImage: require('@/assets/img/data/top_dream_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_dream_66.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772329534',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  caption:
                    '디페스타 방문 기념으로 ‘Sign Wall’ 앞에서 포토 타임을 가지는 엔시티 드림의 모습. 현장을 가득 채운 시즈니들이 반가운 드림이들. 빠질 수 없는 첫번째 코스 Sign Wall에서의 포토타임을 가졌다. 현장에 모인 시즈니들과 인사 후 ‘THE EXPERIENCE’로 이동! 미디어존에서 나오는 ‘HELLO FUTURE’에 해찬은 존 설명을 멈추고 올라오는 흥에 몸을 흔들었다. 야무지게 챌린지 촬영까지 끝낸 후 XR 무대를 보기 위해 영화관으로 이동하는 엔시티 드림. 오랜만에 온 영화관에 들뜬 모습이었다. 본격적으로 영화가 시작되자 동시에 나레이션을 따라 읽는 장난기 가득한 드림이들. 영화 같은 영상미에 입을 다물지 못하는 멤버들. 영상으로 보니 새록새록 떠오르는 그날의 기억들과 오랜만에 보는 런쥔의 긴머리에 말을 잇지 못하는 마크였다. 또한 관객의 입장으로 보는 거 같아 새로운 경험을 해 기분이 좋다는 소감 또한 남겼다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00066_still_01.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00066_still_02.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00066_still_03.jpg'),
                    },
                  ],
                  caption:
                    '레드카펫에서 멋진 워킹 후 다음으로 69명 아티스트가 전시된 EXHIBITION 공간에 도착하였다. 멤버들은 마크 사진에 남겨진 ‘난 네가 좋다’ 사랑고백을 발견 후 범인 맞추기에 열심이었다. 모두 메시지 작성 후 열심히 인증사진도 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00066_still_04.jpg'),
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00066_still_05.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00066_still_06.jpg'),
                  caption:
                    '걷다 보니 디스패치와 DREAM이 함께한 추억들로 가득한 메모리 룸에 도착하였다. 들어가자마자 호버보드 타던 뽀짝 시절 사진을 맞이하며 폭소하는 엔시티 드림. 수많은 사진들을 보며 모두 추억여행에 빠졌다',
                  caption2:
                    '그리고 이어진 또 다른 화보 사진들이 있는 공간! 보고 또 봐도 예쁜 단체화보를 지나 활짝 웃으며 사진도 남겼다. 또한 사진 옆에 시즈니들을 위한 사인과 메시지도 남겼다. 벌써 THE EXHIBITION의 마지막 코스로 직접 그린 대형 캔버스 작품이 있는 공간에 도착하였다. 캔버스 제작 영상 속에 건강을 적는 막내 ‘지성’을 보고 모두 빵 터지는 모습이었다. 전시의 꽃 굿즈샵에 입장한 드림이들. 해찬이가 파일 굿즈 중 샘플을 담자 당황하는 스텝들. 폭소하는 다른 멤버들이었다. 이제는 헤어질 시간으로 시즈니들을 향한 인사를 마지막으로 런쥔은 디페스타를 통해 함께 했던 시간들을 다 찾아볼 수 있어 좋았던 시간이라고 말을 남겼으며, 마크는 한번보고 끝나는 것이 아닌, 계속해서 보고싶은 전시회 인 것 같다며 뜻 깊은 소감을 남겼다.',
                },
              ],
            },
          ],
        },
      },
      {
        id: '00067',
        thumb: require('@/assets/img/data/spot_thumbnail_b_dream_67.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_dream_67.jpg'),
        title: `귀요미 멤버 총집합! <br> 디페스타의 <br> 마지막을 장식할 트리플J <br> ‘NCT DREAM’`,
        text: `지난 5월 17일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’에 시즈니들의 환호를 받으며 NCT DREAM의 트리플 J 재민, 지성, 제노가 깜짝 방문하였다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_dream.jpg'),
          moCoverlImage: require('@/assets/img/data/top_dream_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_dream_67.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772329534',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  thumb: require('@/assets/img/data/00067_still_01.jpg'),
                  caption: '첫번째 순서는 SING WALL 앞에서 갖는 즐거운 포토타임.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00067_still_02.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00067_still_03.jpg'),
                  caption:
                    '지성까지 촬영완료 후 사인월에 이어 THE EXPERIENCE의 새로운 미디어 공간으로 이동. 거울 셀카 장인 지성이의 감독 아래 야무지게 사진을 남기는 엔시티 드림이들',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00067_still_04.jpg'),
                  caption:
                    '‘HELLO FUTURE’ 챌린지까지 신나게 즐기고 기대감 잔뜩 품으며 이동 중인 엔시티 드림. 영화관으로 가는 길목 곳곳의 디페스타 포스터에 제노는 “많은 아티스트분들이(계시네요)” 라며 눈을 떼지 못하였다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00067_still_05.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00067_still_06.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00067_still_07.jpg'),
                    },
                  ],
                  caption:
                    '여기저기 구경하며 영화관에 도착하였다. 첫번째 관전 포인트인 디페스타 버전 댄스브레이크! 를 지나 두번째 관전 포인트인 노래와 찰떡인 LED배경을 감상하고 마지막으로 머리 긴 런쥔이를 보며 웃음 터진 멤버들.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00067_still_08.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00067_still_09.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00067_still_10.jpg'),
                    },
                  ],
                  caption:
                    '디페스타의 두번째 공간인 THE EXHIBITION에 도착하였다. 서로의 사진에 메시지를 담기 위해 눈과 손이 바쁜 멤버들이었다. 지성은 “오늘의 미션은 티 나지 않게 메시지 남기기”라며 장꾸력 넘치는 모습을 보여줬다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00067_still_11.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00067_still_12.jpg'),
                  caption:
                    '다음으로 69명의 아티스트와 디스패치의 역사가 담긴 MEMORY ROOM에 입장했다. 입장과 동시에 눈에 띈 호버보드를 타고 신난 애기 런쥔의 사진을 지나, 지성이 밖에 다니기 창피해했던 인간 파워에이드 시절 사진을 감상하며 메모리 룸에서의 또다른 새로운 추억을 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00067_still_13.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00067_still_14.jpg'),
                  caption:
                    '팀별 콘셉트 화보사진이 있는 곳으로 이동! 오늘 오지 못한 멤버들을 위한 빠질 수 없는 인증샷 찰칵',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00067_still_15.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00067_still_16.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00067_still_17.jpg'),
                    },
                  ],
                  caption:
                    '멤버들이 직접 작업한 페인팅을 본격적으로 감상하기 전 비하인드 영상을 보는 드림이들. 진지하게 그때의 추억을 회상하며 굿즈샵으로 넘어가기 전 마지막 포토타임을 가졌다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00067_still_18.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00067_still_19.jpg'),
                  caption:
                    '굿즈샵에 들어서자 재민은 “아니 이렇게 MD로 만들어질 줄 알았으면 더 예쁘게 했어야 하는데 그치?” 라며 직접 만든 작업물이 굿즈로 제작 된 것에 대한 놀라움을 감추지 못하였다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00067_still_20.jpg'),
                  caption:
                    '마지막으로 야무지게 마스킹 테이프까지 챙겨 마지막 코스 굿즈 쇼핑 완료! 지성은 “볼거리가 많아 다양한 방면에서 즐겼던 것 같고, 팬분들과의 호흡을 경험했다.” 라며 잊지 못할 추억을 만들어 가는 모습이었다. 제노는 “페인팅 그림을 보고 더 예쁘게 그릴 걸 하는 아쉬움이 남지만 예쁘게 만들어져 다행”이라는 재미있는 소감을 마지막으로 행사를 마무리했다.',
                },
              ],
            },
          ],
        },
      },
    ],
  },
  {
    name: 'nct127',
    artistName: 'nct 127',
    nowPost: [
      {
        id: '00060',
        thumb: require('@/assets/img/data/spot_thumbnail_b_nct_60.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_nct_60.jpg'),
        title: `D’FESTA를 찾은 <br> 첫 번째 주인공 ‘NCT127’`,
        text: `지난 4월 8일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’가 화려한 시작을 알렸다. <br> ‘NCT127’의 쟈니, 유타, 태용이 제일 먼저 디페스타 VIP 시사회 참석해 영화와 사진전을 관람하며, 자리를 빛냈다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_nct.jpg'),
          moCoverlImage: require('@/assets/img/data/top_nct_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_nct_60.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772329726',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  thumb: require('@/assets/img/data/00060_still_01.jpg'),
                  caption: '팬들과 반가운 인사를 나누며, 영화관으로 입장한 쟈니, 유타, 태용.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00060_still_02.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00060_still_03.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00060_still_04.jpg'),
                    },
                  ],
                  caption:
                    '영화 같은 전개와 화려한 영상에 점점 빠져들기 시작한 멤버들은 촬영했을 때를 떠올리며 즐거워했다. 또한 화려한 XR 기술에 감탄하던 멤버들은 ‘영웅’ 무대가 끝나자 기립 박수를 보내는 개구진 모습을 보여줬다.',
                },
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00060_still_05.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00060_still_06.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00060_still_07.jpg'),
                    },
                  ],
                  caption:
                    '다음으로 레드카펫을 지나, 두 번째 테마인 THE EXHIBITION 전시 공간에 도착했다. 입장하자마자 찾은 첫 메시지의 주인공은 함께 오지 못한 ‘도영’이었다. 도영을 뒤로 다른 멤버들을 찾아 메시지는 물론 네일 아트까지 남기는 섬세함을 보여줬다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00060_still_08.jpg'),
                  caption: `자연스럽게 추억 여행을 떠날 수 있는 ‘MEMORY ROOM’을 지나 콘셉트 화보 앞에 도착했다. 유타의 “우리 다 잘 생겼다”라는 감상평은 하나의 작품이 된 화보에 대한 팬들의 기대감을 상승시키기에 충분했다.`,
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00060_still_09.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00060_still_10.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00060_still_11.jpg'),
                    },
                  ],
                  caption: '시즈니들을 위한 사진도 남기고 각자 인증샷도 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00060_still_12.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00060_still_13.jpg'),
                  caption: `마지막으로 ’ARTIST PAINTING’ 섹션에서 멤버들과 함께 완성한 그림을 보며 인증샷을 남겼다. 시간 관계상 ‘THE EXPERIENCE’는 관람하지 못 했지만, 본인들처럼 즐겁게 관람할 팬들에게 ‘디페스타에 대한 기대와 감사 인사’를
                  전하며 VIP 시사회를 마무리했다.`,
                },
              ],
            },
          ],
        },
      },
      {
        id: '00061',
        thumb: require('@/assets/img/data/spot_thumbnail_b_nct_61.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_nct_61.jpg'),
        title: `그 누구보다 빠르게, <br> 남들과는 다르게 <br> D’FESTA를 찾은 <br> ‘NCT127’`,
        text: `지난 4월 12일 ‘디페스타(D’FESTA)’를 ‘NCT127’의 재현, 정우, 도영, 태일이 찾았다. VIP 시사회의 첫 주인공이 되지는 못했지만 그 누구보다 빠르게, 남들과는 다르게 디페스타를 즐겼다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_nct.jpg'),
          moCoverlImage: require('@/assets/img/data/top_nct_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_nct_61.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772329726',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  thumb: require('@/assets/img/data/00061_still_01.jpg'),
                  caption:
                    '디페스타 VIP 시사회를 본격적으로 시작하기 전, 대형 ‘Sign Wall’에서 포토타임을 가졌다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00061_still_02.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00061_still_03.jpg'),
                  caption: `독특한 스토리라인과 화려한 영상으로 완성된 ‘THE MOVIE’ 관람을 위해 영화관으로 이동했다. 영화를 감상하던 도영은 “사운드가 다르다”며 고퀄리티 영상에 감탄했고 영화가 끝나자 멤버들 모두 박수갈채를 보냈다.`,
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00061_still_04.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00061_still_05.jpg'),
                  caption: `“K-POP의 현재 같은데?”라는 도영의 말과 함께 도착한 ‘THE EXHIBITION’. 9그룹, 69명의 컬러와 흑백 사진 속 자신과 다른 멤버들을 찾느라 눈도 바쁘고, 메시지를 남기느라 손도 바쁜 시간을 보냈다.`,
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00061_still_06.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00061_still_07.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00061_still_08.jpg'),
                    },
                  ],
                  caption:
                    '그리고 ‘거울의 방’을 연상케 하는 ‘MEMORY ROOM’에 도착했다. 갑자기 시작된 ‘제일 오래된 사진 찾기’는 눈썰미 좋은 멤버들 덕에 1초 컷으로 종료. 그 후 자유롭게 구경했다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00061_still_09.jpg'),
                },
              ],
            },
            {
              type: 'gallery',
              gallery: [
                {
                  thumb: require('@/assets/img/data/00061_still_10.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00061_still_11.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00061_still_12.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00061_still_13.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00061_still_14.jpg'),
                },
              ],
              galleryCaption:
                '그룹별 콘셉트 화보가 있는 공간으로 이동해 단체 사진과 개인 사진 등 촬영하고, 팬들을 위해 사인도 정성스레 남겼다.',
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00061_still_15.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00061_still_16.jpg'),
                  caption: `멤버들과 함께 그린 작품을 구경한 후 이동한 굿즈 샵에서는 누구보다 열정적인 모습으로 멤버들 굿즈를 쇼핑하는 ‘NCT127’이었다. 그리고 마지막으로 ‘함께한 시간을 공유할 수 있었다며’는 감사 인사를 전하며 VIP 시사회를 마쳤다.`,
                },
              ],
            },
          ],
        },
      },
    ],
  },
  {
    name: 'twice',
    artistName: 'twice',
    nowPost: [
      {
        id: '00068',
        thumb: require('@/assets/img/data/spot_thumbnail_b_twice_68.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_twice_68.jpg'),
        title: 'ONE IN A MILLION! <br> D’FESTA를 찾은 <br> 유일무이 걸그룹 <br> ‘트와이스’',
        text: `지난 5월 3일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’에 귀여움의 대명사 ‘트와이스’ 미나, 다현, 채영이 신나게 디페스타를 즐기기 위해 발걸음 했다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_twice.jpg'),
          moCoverlImage: require('@/assets/img/data/top_twice_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_twice_68.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772330202',
          },
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList',
              stillList: [
                {
                  thumb: require('@/assets/img/data/00068_still_01.jpg'),
                  caption: '디페스타 방문 기념으로 ‘Sign Wall’ 앞에서 포토 타임을 가지기로!',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00068_still_02.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_03.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_04.jpg'),
                    },
                  ],
                  caption:
                    '간단한 포토타임 후 미디어 무대를 직접 즐길 수 있는 EXPERIENCE 존으로 향한 트와이스. 거울사진 맛집 답게 옹기종기 모여 함께 인증샷도 찰칵~! 찍고, 신나는 분위기에 몸을 맡겨 댄스 첼린지까지 완료!',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00068_still_05.jpg'),
                  caption:
                    '‘THE MOVIE’로 이동 중인 길목, 디페스타 축제답게 곳곳엔 9팀의 사진이 가득하다. 영화관 앞에도 9팀의 아티스트 모습이 담겨 있는 배너를 볼 수 있었다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00068_still_06.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_07.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_08.jpg'),
                    },
                  ],
                  caption:
                    '설레는 마음을 안고 영화관 안으로 입장! 즐겁게 무대를 관람하는 멤버들. 미나는 “오프닝 영상부터 스케일도 크고 설레는 영상이었는데 예쁘고 멋있게 찍어주셔서 한 편의 영화를 보는 것 같은 기분”이라는 센스있는 소감을 남겼다. 채영 또한 XR무대를 보고 어벤져스 집 같다며 그만큼 고퀄의 무대라 재미있었다고 감탄했다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00068_still_09.jpg'),
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00068_still_10.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_11.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_12.jpg'),
                    },
                  ],
                  caption:
                    '9팀의 69명 아티스트가 전시된 EXHIBITION 공간에 도착했다. 오늘 트와이스는 각자의 개성을 살린 뽀짝한 그림으로 방문흔적을 남길 예정. 트와이스의 첫번째 메시지는 다현이 거침없이 채영 사진에 남긴 “내가 바로 딸기공쥬” 였다. 다현의 사진에는 채영이 열심히 손을 뻗어 그린 귀여운 날개가 생겼다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00068_still_13.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_14.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_15.jpg'),
                    },
                  ],
                  caption:
                    '다음장소는 9팀의 아티스트와 디스패치의 추억이 담긴 MEMORY ROOM. 입이 떡 벌어지는 놀라움도 잠시 입구에서부터 트와이스를 폭소케 하는 사진이 눈에 띄었다. 데뷔 초 장꾸력 넘쳤던 트둥이들의 얼굴에 스티커가 붙은 사진을 시작으로 새록새록 떠오르는 그날의 기억들을 함께 되돌아보는 시간을 가졌다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00068_still_16.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_17.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_18.jpg'),
                    },
                  ],
                  caption:
                    '화보 사진 감상 후 방문하는 원스들을 위해 사인과 코멘트도 남기고, 오늘 함께 오지 못한 멤버들을 위해 빠질 수 없는 셀카 인증샷 타임도 가졌다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00068_still_19.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_20.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_21.jpg'),
                    },
                  ],
                  caption:
                    'THE EXHIBITION의 마지막, 그림 그리는 과정을 담은 영상을 포함한 9팀이 직접 그린 대형 캔버스 작품이 전시되어 있는 공간에 도착하였다. 원래는 새하얗던 캔버스를 함께 채워 나간 멤버들. 다현은 흰 캔버스에 맨 처음 트와이스를 썼던 때를 회상했다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00068_still_22.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00068_still_23.jpg'),
                  caption:
                    '전시장에서 보니 색다른 작품을 감상하는 트와이스. 채영은 핸드폰을 들어 개인 소장용으로 사진을 남겼다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00068_still_24.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_25.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00068_still_26.jpg'),
                    },
                  ],
                  caption:
                    '마지막 코스는 디페스타를 기념할 수 있는 굿즈샵. 볼펜도 담고 본격적으로 굿즈 쇼핑을 나선 멤버들.마지막으로 채영은 예쁜 공간에 멋있게 디페스타를 만들어줘서 고맙다는 말을 남긴 후 “우리가 10주년 된 것 같은데?” 라며 그만큼 트와이스의 추억이 많이 담긴 디페스타에 대한 애정 어린 멘트를 드러내며 행사를 마무리하였다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00068_still_27.jpg'),
                },
              ],
            },
          ],
        },
      },
    ],
  },
  {
    name: 'seventeen',
    artistName: 'seventeen',
    nowPost: [
      {
        id: '00001',
        thumb: require('@/assets/img/data/spot_thumbnail_b_svt_01.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_svt_01.jpg'),
        title: 'SAY THE NAME, SEVENTEEN!<br> 빛내줄게 ‘세븐틴’',
        text: `지난 4월 26일 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’에 세븐틴의 조슈아, 준, 호시, 버논, 승관, 디노가 디페스타를 즐기기 위해 즐겁게 발걸음 하였다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_seventeen.jpg'),
          moCoverlImage: require('@/assets/img/data/top_seventeen_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_svt_01.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772330531',
          },
          moduleList: [
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00001_still_01.jpg'),
                  caption: '모든 아티스트의 첫 필수 코스 ‘Sign Wall’ 앞에서 포토 타임을 가지기로!',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00001_still_02.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00001_still_03.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00001_still_04.jpg'),
                    },
                  ],
                  caption:
                    '간단한 포토타임 후 미디어 무대를 직접 즐길 수 있는 EXPERIENCE 존으로 향한 세븐틴. 거울과 LED의 콜라보 공간에서 거울 사진 장인 호시의 카메라로 옹기종기 모여 함께 인증샷도 남기고, 신나는 분위기에 몸을 맡겨 댄스 챌린지까지 완료!',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00001_still_05.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00001_still_06.jpg'),
                  caption:
                    '다음은 디페스타의 꽃 ‘THE MOVIE’에 설레는 마음을 안고 영화관으로 입장한 멤버들. 무비가 시작되자 영화같은 오프닝에 집중도가 상승하여 진지 모드로 관람하였다. 영상을 보니 360도 라이트케이지 촬영 기억이 새록새록 나는 세븐틴. 본격 떼창 구간에서는 모두 손을 흔들며 감상하는 귀여운 모습을 볼 수 있었다. XR 무대가 시작되자 감탄이 절로 나오는 인트로에 눈이 휘둥그레지는 멤버들과 조슈아는 “이게 XR이라는 게 믿기지가 않아”라며 놀라운 기색을 감추지 못했다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00001_still_07.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00001_still_08.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00001_still_09.jpg'),
                    },
                  ],
                  caption:
                    '무대만큼 기대되는 화보 전시 타임. 9팀의 69명 아티스트가 전시된 EXHIBITION 공간에 도착했다. 이날 세븐틴은 각자의 개성을 살린 메시지와 그림으로 방문 흔적을 남겼다. 13명의 멤버답게 서로의 사진을 찾느라 눈과 손이 모두 바쁜 세븐틴.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00001_still_10.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00001_still_11.jpg'),
                  caption:
                    '다음 장소는 디스패치와 세븐틴의 추억이 가득한 MEMORY ROOM. 방 곳곳에서 멤버들을 발견할 때마다 추억이 새록새록. 귀요미 시절의 디노를 발견한 조슈아는 “이때는 진짜 아기 같다”며 함께했던 추억을 회상하였다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00001_still_12.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00001_still_13.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00001_still_14.jpg'),
                    },
                  ],
                  caption:
                    '추억 여행이 끝난 후 단체 화보를 감상하며 디페스타를 방문하는 캐럿들을 위한 흔적도 남기고, 빠질 수 없는 인증샷 타임도 가졌다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00001_still_15.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00001_still_16.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00001_still_17.jpg'),
                    },
                  ],
                  caption:
                    'THE EXHIBITION의 마지막, 캔버스에 직접 그리는 과정이 담긴 영상 관람 후 9팀이 직접 그린 대형 캔버스가 전시된 공간에 도착하였다. 새하얀 캔버스를 함께 채워 나가는 모습을 관람 후 완성된 작품 앞에서 인증 사진도 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00001_still_18.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00001_still_19.jpg'),
                  caption:
                    '전시회의 묘미 굿즈샵 쇼핑하러 고고! 특히 세븐틴 그림을 본떠 만든 볼펜에 시선이 고정된 세븐틴 멤버들. 엽서는 종류대로 전부 Get! 반면에 캐럿들이 못 살까 봐 걱정하며 굿즈 쇼핑 망설이는 버논의 따듯한 모습도 볼 수 있었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00001_still_20.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00001_still_21.jpg'),
                  caption:
                    '잔뜩 쇼핑한 굿즈들을 에코백에 담고서 마무리 인사를 남기는 세븐틴 멤버들. 승관은 “진짜 이런 자리가 또 있을까 싶은데 너무 뜻깊었고, 오늘 초대해 주셔서 감사드린다”는 소감을 남기며 행사를 마무리하였다.',
                },
              ],
            },
          ],
        },
      },
      {
        id: '00071',
        thumb: require('@/assets/img/data/spot_thumbnail_b_svt_71.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_svt_71.jpg'),
        title: '13+3+1=17<br> 같이 가요 ‘세븐틴’',
        text: `지난 4월 26일 오후, 글로벌 K-POP 페스티벌 ‘디페스타(D’FESTA)’의 뜨거운 현장을 느끼러 세븐틴의 민규, 우지, 에스쿱스, 정한, 디에잇, 도겸이 방문하였다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_seventeen.jpg'),
          moCoverlImage: require('@/assets/img/data/top_seventeen_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_svt_71.jpg'),
          videoOptions: {
            isVimeo: true,
            id: '772330531',
          },
          moduleList: [
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_01.jpg'),
                  caption:
                    '첫 번째 코스인 디페스타 사인 월 앞에서 포토타임 갖는 세븐틴의 모습. 단체에서 개인 포토 타임 후 THE EXPERIENCE 존으로 이동!',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_02.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00071_still_03.jpg'),
                  caption:
                    '민규는 “원래 이런 곳에 오면 인증샷을 꼭 찍어야 한다”며 멤버들과 모여 거울 샷을 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_04.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00071_still_05.jpg'),
                  caption:
                    '인증샷을 남기고 잔뜩 신이 난 흥븐틴은 둠칫둠칫 몸을 흔들며 댄스 챌린지를 즐겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_06.jpg'),
                  caption:
                    '설렘 속 시작되는 영상 직전 단체사진 찰칵! 영상이 시작되자 세개의 유형으로 무대를 즐기는 모습이 나눠지는 멤버들. 유형1. 노래에 맞춰 춤 추기, 유형2. 휴대폰 플래쉬 흔들기, 마지막으로 승관이를 사랑하는 유형(=도겸)까지..한 영상에서도 다양한 리액션을 보여주는 끼 넘치는 세븐틴.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_07.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00071_still_08.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00071_still_09.jpg'),
                  caption:
                    'XR 무대가 시작되자 멤버들의 눈은 휘둥그레지고 어느새 감탄으로 가득 찬 영화관. 화려한 XR 기술에 도겸은 벌떡 일어나 기립박수로 호응했다. 디에잇은 “상상도 못 할 만큼 너무 좋은 프로젝트”라며 감사하다는 말을 전하였다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_10.jpg'),
                  caption:
                    '모델처럼 레드카펫을 지나, 이어지는 THE EXHIBITION에 도착! 들어가자마자 멤버들을 맞이하는 “버논”에게 코멘트를 남기는 멤버들.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_11.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00071_still_12.jpg'),
                  caption:
                    '리더 ‘에스쿱스’를 놀릴 생각에 신이 난 ‘민규’는 에스쿱스 화보에 타투 낙서를 하며 센 리더로 변신시켰다. 또 민규의 키를 재서 표시한 캐럿 들의 예비 핫 스팟도 완성!',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_13.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00071_still_14.jpg'),
                  caption:
                    '디스패치와 함께한 세븐틴의 7년이 담겨있는 memory room에 도착하였다. 감탄사도 잠시, 들어가자마자 반겨주는 신인 쿱스의 사진에 빵 터진 멤버들. 이런 걸 또 놓칠 리 없는 장난 꾸러기 민규는 핸드폰 카메라를 들어 사진으로 남겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_15.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00071_still_16.jpg'),
                  caption:
                    '이어서 펼쳐지는 단체 사진을 배경으로 인증샷도 찍고 사인과 코멘트도 남겼다.',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00071_still_17.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00071_still_18.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00071_still_19.jpg'),
                    },
                  ],
                  caption:
                    'THE EXHIBITION의 마지막이자 9팀의 그림을 모두 감상할 수 있는 곳에 도착! 다 함께 참여한 캔버스 작업 영상 관람 후, 야무지게 인증샷도 남기고 마지막 코스인 굿즈샵으로 이동!',
                },
              ],
            },
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00071_still_20.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00071_still_21.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00071_still_22.jpg'),
                    },
                  ],
                  caption:
                    '예쁜 엽서 잔뜩 챙기고, 오늘 못 온 원우 엽서도 담는 멤버들. 우지는 부모님께 드릴 엽서도 종류별로 챙기며 효자의 면모를 보여주었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00071_still_23.jpg'),
                  caption:
                    '이렇게 디페스타 투어가 완료되었다. 리더 에스쿱스는 “K-POP을 사랑하는 전 세계의 많은 분이 오셔서 구경하시고, 즐기시고, 행복감을 얻어가셨으면 좋겠다”는 소감을 마지막으로 행사를 마무리하였다.',
                },
              ],
            },
          ],
        },
      },
    ],
  },
  {
    name: 'bts',
    artistName: 'bts',
    nowPost: [],
  },
  {
    name: 'nuest',
    artistName: 'nu’est',
    nowPost: [
      {
        id: '00069',
        thumb: require('@/assets/img/data/spot_thumbnail_b_nuest_69.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_nuest_69.jpg'),
        title: '지금의 넌 <br> 너의 계절에 살고있어 <br> ‘뉴이스트’ - 민현',
        text: `지난 4월 13일 글로벌 K-POP 페스티벌 ‘디페스타 (D’FESTA)’에 뉴이스트 민현이 팬들도 모르게 깜짝 방문하였다.`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_nu.jpg'),
          moCoverlImage: require('@/assets/img/data/top_nu_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_nuest_69.jpg'),
          videoOptions: null,
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'puzzleList',
              puzzleList: [
                {
                  list: [
                    {
                      thumb: require('@/assets/img/data/00069_still_01.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00069_still_02.jpg'),
                    },
                    {
                      thumb: require('@/assets/img/data/00069_still_03.jpg'),
                    },
                  ],
                  caption:
                    '설레는 방문 직후 바로 영화관으로 직행한 뉴이스트 민현. 들어가서 바로 뒷 자석에 착석하자 팬들은 민현이 닮았다 라며 뒷자석을 응시하였다. 마스크와 모자로 가려져도 멋짐이 새어 나오는 민현이었다.',
                  caption2:
                    '팬들과 함께 뉴이스트 영상 관람 후 전시의 중심인 EXHIBITION에서 멋지게 사인과 메시지도 남겼다. 함께 영화를 관람한 팬들은 놀라움을 감추지 못하며 민현과 함께 전시를 관람하였다. 디페스타 스탭이 챙겨준 목걸이와 에코백을 받고 주섬주섬 목걸이를 맨 민현은 굿즈샵에서 본인사진과 멤버 단체사진이 들어간 엽서 모두 야무지게 챙겼다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00069_still_04.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00069_still_05.jpg'),
                  caption:
                    '마지막으로 EXPERIENCE 존에서 뉴이스트의 영상을 관람하며 새록새록 떠오르는 맴버들과의 추억을 간직한체 행사를 마무리하였다.',
                },
              ],
            },
          ],
        },
      },
      {
        id: '00070',
        thumb: require('@/assets/img/data/spot_thumbnail_b_nuest_70.jpg'),
        mainThumb: require('@/assets/img/data/spot_thumbnail_a_nuest_70.jpg'),
        title: '지금의 난 <br> 너의 계절에 살고있어 <br> ‘뉴이스트’ - 백호',
        text: `지난 4월 19일 글로벌 K-POP 페스티벌 ‘디페스타 (D’FESTA)’에 뉴이스트 백호가 흰티에 청바지의 케주얼하지만 설레는 차림으로 깜짝 방문한 디페스타!`,
        nowDetails: {
          coverlImage: require('@/assets/img/data/top_nu.jpg'),
          moCoverlImage: require('@/assets/img/data/top_nu_m.jpg'),
          detailCover: require('@/assets/img/data/spot_cover_nuest_70.jpg'),
          videoOptions: null,
          puzzleList: null,
          gallery: null,
          moduleList: [
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00070_still_01.jpg'),
                },
                {
                  thumb: require('@/assets/img/data/00070_still_02.jpg'),
                  caption:
                    '뉴이스트 목걸이를 목에 매고 디페스타의 THE MOVIE를 관람하러 가는 백호는 가는 길목마다 친절하게 팬들을 향해 인사해주는 스윗한 면모를 보여주었다. 디페스타 영화 관람 후 전시장 내부에서도 팬들과 함께 소통하는 백호의 모습. 아티스트와 함께 전시를 관람하게 된 팬들과 관람객들 모두 믿기지 않는 듯한 반응을 보였다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00070_still_03.jpg'),
                  caption:
                    'EXHIBITION이 끝나고 굿즈존으로 나온 백호. 맴버들과 함께 촬영한 작업물들이 가득 들어간 도록, 엽서, 파일을 야무지게 쇼핑 후 에코백에 넣었다.',
                },
              ],
            },
            {
              type: 'stillList2',
              stillList2: [
                {
                  thumb: require('@/assets/img/data/00070_still_04.jpg'),
                  caption:
                    '마지막까지 팬들 모두에게 각각 눈 맞춰주는 백호의 훈훈한 모습을 마지막으로 행사는 마무리 되었다.',
                },
              ],
            },
          ],
        },
      },
    ],
  },
]

export default nowData
