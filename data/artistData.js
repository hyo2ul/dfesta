const artistData = [
  {
    name: 'enhypen',
    artistName: 'ENHYPEN',
    thumb: require('@/assets/img/data/t01.jpg'),
    logo: require('@/assets/img/logo_artist_enhypen.svg'),
    keyvisual: {
      title: 'ENHYPEN',
      img: require('@/assets/img/data/c01.jpg'),
    },
    description: {
      title: '새로운 세계를 연결하며 <br> 성장하는 엔하이픈',
      text: '그룹명인 ENHYPEN은 단어와 단어를 연결해 문장을 만드는 하이픈(-)처럼 연결을 통해 발견하고 성장하라는 의미를 담고 있다. I-LAND를 통해 준비생과 아티스트라는 상반된 세계를 연결하는 과정을 거쳐왔고 더 나아가서는 아티스트로서 사람과 사람, 세대와 세대를 연결하고 분열된 세상을 연결하라는 뜻을 담고 있다. 약칭은 EN-.',
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_enhypen-01.jpg'),
        name: 'JUNGWON',
      },
      {
        thumb: require('@/assets/img/data/color_enhypen-02.jpg'),
        name: 'HEESEUNG',
      },
      {
        thumb: require('@/assets/img/data/color_enhypen-03.jpg'),
        name: 'JAY',
      },
      {
        thumb: require('@/assets/img/data/color_enhypen-04.jpg'),
        name: 'JAKE',
      },
      {
        thumb: require('@/assets/img/data/color_enhypen-05.jpg'),
        name: 'SUNGHOON',
      },
      {
        thumb: require('@/assets/img/data/color_enhypen-06.jpg'),
        name: 'SUNOO',
      },
      {
        thumb: require('@/assets/img/data/color_enhypen-07.jpg'),
        name: 'NI-KI',
      },
    ],
    lyrics:
      '아름답고 황홀해 반짝이는 내 각막은 다이아몬드 규칙 없는 세계는 전부 뒤집혀 뒤집혀 서있어',
    lyricsDuration: 30,
    song: {
      title: 'DRUNK - DAZED',
      date: 'TAMED - DASHED',
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/en01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/en02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/en03.jpg'),
      },
    ],
  },
  {
    name: 'txt',
    artistName: 'TXT',
    thumb: require('@/assets/img/data/t02.jpg'),
    logo: require('@/assets/img/logo_artist_txt.svg'),
    keyvisual: {
      title: 'txt',
      img: require('@/assets/img/data/c02.jpg'),
    },
    description: {
      title: '서로 다른 너와 내가 하나의 꿈 <br> 투모로우바이투게더',
      text: `수빈, 연준, 범규, 태현, 휴닝카이 다섯 멤버로 구성된 투모로우바이투게더는 ‘서로 다른 너와 내가 하나의 꿈으로 모여 함께 내일을 만들어간다'라는 뜻으로 하나의 꿈과 목표를 위해 함께 모인 소년들이 서로 시너지를 발휘하는 밝고 건강한 아이돌 그룹이다.`,
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_txt-01.jpg'),
        name: 'SOOBIN',
      },
      {
        thumb: require('@/assets/img/data/color_txt-02.jpg'),
        name: 'YEONJUN',
      },
      {
        thumb: require('@/assets/img/data/color_txt-03.jpg'),
        name: 'BEOMGYU',
      },
      {
        thumb: require('@/assets/img/data/color_txt-04.jpg'),
        name: 'TAEHYUN',
      },
      {
        thumb: require('@/assets/img/data/color_txt-05.jpg'),
        name: 'HUENINGKAI',
      },
    ],
    lyrics: `세계의 유일한 법칙 나를 구해줘 내 손을 잡아줘 Please use me like a drug (I know I love you)`,
    lyricsDuration: 40,
    song: {
      title: '0X1=LOVESONG (I KNOW I LOVE YOU)',
      date: 'LO$ER=LO♡ER',
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/txt01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/txt02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/txt03.jpg'),
      },
    ],
  },
  {
    name: 'straykids',
    artistName: 'STRAY KIDS',
    thumb: require('@/assets/img/data/t03.jpg'),
    logo: require('@/assets/img/logo_artist_straykids.svg'),
    keyvisual: {
      title: 'stray kids',
      img: require('@/assets/img/data/c03.jpg'),
    },
    description: {
      title: '똑같은 길 보다 우리들만의 길로 <br> 스트레이 키즈',
      text: "3RACHA의 랩/힙합을 기반으로 한 프로듀싱과 보컬라인(일명 보컬라차: 승민, 아이엔)의 서정적인 보컬, 댄스라인(댄스라차: 리노, 현진, 필릭스)을 중심으로 한 다이나믹한 퍼포먼스가 합해져 8명이 유기적인 조화를 이루는 것이 이 그룹의 특징. 그룹명인 스트레이키즈(Stray Kids)는 '방황하는 아이들' 이라는 뜻으로, 기존의 틀에서 벗어나 8명이 똘똘 뭉쳐 자신을 길을 스스로 찾아 나가겠다는 포부를 담고 있다.",
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_skz-01.jpg'),
        name: 'BANG CHAN',
      },
      {
        thumb: require('@/assets/img/data/color_skz-02.jpg'),
        name: 'LEE KNOW',
      },
      {
        thumb: require('@/assets/img/data/color_skz-03.jpg'),
        name: 'CHANGBIN',
      },
      {
        thumb: require('@/assets/img/data/color_skz-04.jpg'),
        name: 'HYUNJIN',
      },
      {
        thumb: require('@/assets/img/data/color_skz-05.jpg'),
        name: 'HAN',
      },
      {
        thumb: require('@/assets/img/data/color_skz-06.jpg'),
        name: 'FELIX',
      },
      {
        thumb: require('@/assets/img/data/color_skz-07.jpg'),
        name: 'SEUNGMIN',
      },
      {
        thumb: require('@/assets/img/data/color_skz-08.jpg'),
        name: 'I.N',
      },
    ],
    lyrics: `Here they come 악당 무리에 뜨거운 피가 돌아 온몸에 번져 소문난 꾼들의 모임에 쏟아지는 눈빛은 Freezing cold but I know we'll burn forever 해보라는 태도 난 여전히 할 말을 내뱉지 (퉤 퉤 퉤)`,
    lyricsDuration: 95,
    song: {
      title: 'THUNDEROUS',
      date: 'GOD’S MENU D’FESTA VER.',
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/sk01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/sk02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/sk03.jpg'),
      },
    ],
  },
  {
    name: 'nctdream',
    artistName: 'NCT DREAM',
    thumb: require('@/assets/img/data/t04.jpg'),
    logo: require('@/assets/img/logo_artist_nctdream.svg'),
    keyvisual: {
      title: 'nct dream',
      img: require('@/assets/img/data/c04.jpg'),
    },
    description: {
      title: '비상하는 NCT의 꿈 <br> 엔시티 드림',
      text: 'NCT DREAM은 2016년 8월 25일 데뷔한 SM엔터테인먼트 소속 7인조 다국적 보이그룹 이며, NCT에서 세 번째로 데뷔한 유닛이다. <br><br> 10대 멤버들로 이루어진 청소년 연합팀으로 데뷔했으며, “10대들에게 꿈과 희망을, 성인들에게는 힐링을” 주는 것을 포부로 하는 그룹이다. 만 나이 기준 20세가 지나면 NCT DREAM을 졸업하는 로테이션 형식의 그룹으로 기획 되었지만, 2020년 4월 14일 체제를 개편하여 졸업 규정을 삭제하고 기존 멤버 그대로 모두 함께 활동을 이어가고 있다.',
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_nctdream-01.jpg'),
        name: 'MARK',
      },
      {
        thumb: require('@/assets/img/data/color_nctdream-02.jpg'),
        name: 'RENJUN',
      },
      {
        thumb: require('@/assets/img/data/color_nctdream-03.jpg'),
        name: 'JENO',
      },
      {
        thumb: require('@/assets/img/data/color_nctdream-04.jpg'),
        name: 'HAECHAN',
      },
      {
        thumb: require('@/assets/img/data/color_nctdream-05.jpg'),
        name: 'JAEMIN',
      },
      {
        thumb: require('@/assets/img/data/color_nctdream-06.jpg'),
        name: 'CHENLE',
      },
      {
        thumb: require('@/assets/img/data/color_nctdream-07.jpg'),
        name: 'JISUNG',
      },
    ],
    lyrics:
      '너와 내 사이 선명히 보인 커져만 가는 열기 난 오감을 깨워 네 맘을 깨워 터질 것만 같은 ma skill',
    lyricsDuration: 45,
    song: {
      title: 'HOT SAUCE',
      date: 'HELLO FUTURE D’FESTA VER.',
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/dream01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/dream02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/dream03.jpg'),
      },
    ],
  },
  {
    name: 'nct127',
    artistName: 'NCT 127',
    thumb: require('@/assets/img/data/t05.jpg'),
    logo: require('@/assets/img/logo_artist_nct127.svg'),
    keyvisual: {
      title: 'nct 127',
      img: require('@/assets/img/data/c05.jpg'),
    },
    description: {
      title: 'TO THE WORLD, <br> 여기는 엔시티 127',
      text: 'NCT 127의 ‘127’은 서울의 경도를 의미하며, 한국, 일본을 중심으로 활동할 예정이라고 한다. 즉, K-POP의 본거지인 서울을 중심으로 활동하기 때문에 K-POP을 전세계에 알리겠다는 포부를 담았다는 뜻이다.',
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_nct127-01.jpg'),
        name: 'TAEIL',
      },
      {
        thumb: require('@/assets/img/data/color_nct127-02.jpg'),
        name: 'JOHNNY',
      },
      {
        thumb: require('@/assets/img/data/color_nct127-03.jpg'),
        name: 'TAEYONG',
      },
      {
        thumb: require('@/assets/img/data/color_nct127-04.jpg'),
        name: 'YUTA',
      },
      {
        thumb: require('@/assets/img/data/color_nct127-05.jpg'),
        name: 'DOYOUNG',
      },
      {
        thumb: require('@/assets/img/data/color_nct127-06.jpg'),
        name: 'JAEHYUN',
      },
      {
        thumb: require('@/assets/img/data/color_nct127-07.jpg'),
        name: 'MARK',
      },
      {
        thumb: require('@/assets/img/data/color_nct127-08.jpg'),
        name: 'HAECHAN',
      },
      {
        thumb: require('@/assets/img/data/color_nct127-09.jpg'),
        name: 'JUNGWOO',
      },
    ],
    lyrics: `수많은 날들의 같은 장면을 반복한 끝에 어제의 날 무너뜨리고 소리치면 돼 내겐 no more trauma Baby we go wild one two seven squad We ain’t never gonna stop 끝이 안 보여도 가`,
    lyricsDuration: 90,
    song: {
      title: 'KICK IT',
      date: 'MUSIC, DANCE',
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/nct01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/nct02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/nct03.jpg'),
      },
    ],
  },
  {
    name: 'twice',
    artistName: 'TWICE',
    thumb: require('@/assets/img/data/t06.jpg'),
    logo: require('@/assets/img/logo_artist_twice.svg'),
    keyvisual: {
      title: 'twice',
      img: require('@/assets/img/data/c06.jpg'),
    },
    description: {
      title: 'ONE IN A MILLION, <br> 트와이스',
      text: `9인조 다국적 걸그룹으로, 팀명의 의미는 눈으로 한번, 귀로 한번해서 두 번 감동을 준다는 뜻이다. 또한 해외 가수 기준 최단기간 도쿄돔 입성, 미국 빌보드 메인 앨범 차트 '빌보드 200' 3위, 전 세계 걸그룹 가운데 최다인 20편의 억대 뮤직비디오 보유 등의 기록을 갖고 있다.`,
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_twice-01.jpg'),
        name: 'NAYEON',
      },
      {
        thumb: require('@/assets/img/data/color_twice-02.jpg'),
        name: 'MOMO',
      },
      {
        thumb: require('@/assets/img/data/color_twice-03.jpg'),
        name: 'SANA',
      },
      {
        thumb: require('@/assets/img/data/color_twice-04.jpg'),
        name: 'JIHYO',
      },
      {
        thumb: require('@/assets/img/data/color_twice-05.jpg'),
        name: 'MINA',
      },
      {
        thumb: require('@/assets/img/data/color_twice-06.jpg'),
        name: 'DAHYUN',
      },
      {
        thumb: require('@/assets/img/data/color_twice-07.jpg'),
        name: 'CHAEYOUNG',
      },
      {
        thumb: require('@/assets/img/data/color_twice-08.jpg'),
        name: 'TZUYU',
      },
    ],
    lyrics:
      '단 한 번도 느껴본 적 없는 걸 알게 해주는 (사람 기다리고 있는 걸) 얼마가 돼도 기다리고 싶어 I just wanna fall in love ',
    lyricsDuration: 75,
    song: {
      title: 'LIKE OOH-AHH',
      date: 'WHAT IS LOVE? + CHEER UP D’FESTA VER.',
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/tw01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/tw02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/tw03.jpg'),
      },
    ],
  },
  {
    name: 'seventeen',
    artistName: 'SEVENTEEN',
    thumb: require('@/assets/img/data/t07.jpg'),
    logo: require('@/assets/img/logo_artist_seventeen.svg'),
    keyvisual: {
      title: 'seventeen',
      img: require('@/assets/img/data/c07.jpg'),
    },
    description: {
      title: '13+3+1=17 <br> 세븐틴',
      text: "2016년 2월, 데뷔한 지 1년도 안 된 신인이 앨범 두 장으로 가온 차트 기준 누적 17만 장을 팔아치우며 2015년에 데뷔한 남자 아이돌 그룹 중에서 팬덤과 음반면으로 최고의 성적을 거두게 된다. 그 후 2016년 4월에 발매한 첫 번째 정규 앨범 &lt;FIRST 'LOVE&LETTER'&gt;가 초동 8만 장,이때 데뷔 후 1년이 채 되지 않은 채로 2016년 5월4일, 쇼챔피언으로 음방 첫 1위를 달성하게 되고 2016년 12월에 발매한 세 번째 미니 앨범 &lt;Going Seventeen&gt;이 초동 13만 장을 연달아 돌파,2016년 12월16일 뮤직뱅크에서 지상파 첫 1위를 달성한다. 초동 10만 장을 달성한 역대 4번째 보이그룹이 되었다. 당시 쫄딱 망하기 일보 직전이었던 중소 기획사에서 데뷔해 언론의 주목을 그다지 받지 못했던 그룹이 오로지 프리 데뷔와 데뷔 후의 활동만으로 이뤄낸 성과임을 생각하면 이는 대단히 놀라운 성적이라고 할 수 있다.",
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_svt_01.jpg'),
        name: 'S.COUPS',
      },
      {
        thumb: require('@/assets/img/data/color_svt_02.jpg'),
        name: 'JEONGHAN',
      },
      {
        thumb: require('@/assets/img/data/color_svt_03.jpg'),
        name: 'JOSHUA',
      },
      {
        thumb: require('@/assets/img/data/color_svt_04.jpg'),
        name: 'JUN',
      },
      {
        thumb: require('@/assets/img/data/color_svt_05.jpg'),
        name: 'HOSHI',
      },
      {
        thumb: require('@/assets/img/data/color_svt_06.jpg'),
        name: 'WONWOO',
      },
      {
        thumb: require('@/assets/img/data/color_svt_07.jpg'),
        name: 'WOOZI',
      },
      {
        thumb: require('@/assets/img/data/color_svt_08.jpg'),
        name: 'THE 8',
      },
      {
        thumb: require('@/assets/img/data/color_svt_09.jpg'),
        name: 'MINGYU',
      },
      {
        thumb: require('@/assets/img/data/color_svt_10.jpg'),
        name: 'DK',
      },
      {
        thumb: require('@/assets/img/data/color_svt_11.jpg'),
        name: 'SEUNGKWAN',
      },
      {
        thumb: require('@/assets/img/data/color_svt_12.jpg'),
        name: 'VERNON',
      },
      {
        thumb: require('@/assets/img/data/color_svt_13.jpg'),
        name: 'DINO',
      },
    ],
    lyrics: `널 찾아가야 돼, 찾아가야 돼 지금 울면 못 볼지 모르니까 울고 싶지 않아 울고 싶지 않아 눈물은 많지만 울고 싶지 않아`,
    lyricsDuration: 70,
    song: {
      title: 'LeaDON’T WANNA CRYn',
      date: '20, LEAN ON ME, 247',
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/svt01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/svt02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/svt03.jpg'),
      },
    ],
  },
  {
    name: 'bts',
    artistName: 'BTS',
    thumb: require('@/assets/img/data/t08.jpg'),
    logo: require('@/assets/img/logo_artist_bts.svg'),
    keyvisual: {
      title: 'bts',
      img: require('@/assets/img/data/c08.jpg'),
    },
    description: {
      title: '우리가 함께라면 <br> 사막도 바다가 돼. <br> Hello, we are BTS',
      text: '2013년 데뷔해 국내외 신인상을 휩쓴 방탄소년단은 명실상부 한국을 대표하는 최정상 보이 그룹으로 성장했다. 전 세계적으로 방탄소년단 열풍을 일으키며 ‘21세기 팝 아이콘’으로 불린다. 미국 빌보드, 영국 오피셜 차트, 일본 오리콘을 비롯해 아이튠즈, 스포티파이, 애플뮤직 등 세계 유수의 차트 정상에 올랐고, 음반 판매량과 뮤직비디오 조회수, SNS 지수 등에서도 독보적인 기록을 써 내려가고 있다. 특히, 방탄소년단은 한 주에 빌보드 ‘핫 100’ 차트와 ‘빌보드 200’ 차트 정상을 동시 정복한 최초의 그룹이며, 통산 ‘빌보드 200’ 5차례, ‘핫 100’ 5차례 1위를 차지했다. 또한, ‘제63회 그래미 어워드’에서 한국 가수 최초로 단독 무대를 펼쳐 ‘빌보드 뮤직 어워드’와 ‘아메리칸 뮤직 어워드’, ‘그래미 어워드’까지 미국 3대 음악 시상식 무대에서 공연하는 기록을 세웠다. 방탄소년단은 스타디움 투어를 개최하며 전 세계 콘서트 시장에서도 글로벌 아티스트로서의 입지를 다져 왔으며, UN 연설과 LOVE MYSELF 캠페인 등을 통해 선한 영향력을 실천하고 있다.',
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_bts-01.jpg'),
        name: 'RM',
      },
      {
        thumb: require('@/assets/img/data/color_bts-02.jpg'),
        name: 'JIN',
      },
      {
        thumb: require('@/assets/img/data/color_bts-03.jpg'),
        name: 'SUGA',
      },
      {
        thumb: require('@/assets/img/data/color_bts-04.jpg'),
        name: 'J-HOPE',
      },
      {
        thumb: require('@/assets/img/data/color_bts-05.jpg'),
        name: 'JIMIN',
      },
      {
        thumb: require('@/assets/img/data/color_bts-06.jpg'),
        name: 'V',
      },
      {
        thumb: require('@/assets/img/data/color_bts-07.jpg'),
        name: 'JUNG KOOK',
      },
    ],
    lyrics:
      '내 혈관 속 DNA가 말해줘 내가 찾아 헤매던 너라는 걸 우리 만남은 수학의 공식 종교의 율법 우주의 섭리',
    lyricsDuration: 75,
    song: {
      title: 'DNA',
      date: `PERMISSION TO DANCE`,
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/bts01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/bts02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/bts03.jpg'),
      },
    ],
  },
  {
    name: 'nuest',
    artistName: `NU'EST`,
    thumb: require('@/assets/img/data/t09.jpg'),
    logo: require('@/assets/img/logo_artist_nuest.svg'),
    keyvisual: {
      title: `NU'EST`,
      img: require('@/assets/img/data/c09.jpg'),
    },
    description: {
      title: 'NU, Establish, <br> Style, Temp <br> 뉴이스트',
      text: "대한민국의 5인조 보이그룹이자 플레디스엔터테인먼트 최초의 보이그룹으로 멤버는 JR, 아론, 백호, 민현, 렌이다. <br><br> 2012년 3월 15일, 첫 번째 싱글 앨범 'FACE'로 데뷔하였다. 그룹명 NU'EST는 'NU, Establish, Style, Tempo'의 약자이며, 팬클럽 L.O.Λ.E는 뉴이스트의 초성 'ㄴㅇㅅㅌ'에서 따온 것이다.<br><br> 2013년 말 중국인 멤버 제이슨을 영입하여 6인조 유닛 'NU'EST M'으로 중국에서 활동하였으며, 2014년 말에는 일본 오리지널 싱글 앨범을 발매하는 등 국내외로 활발히 활동하였다.",
    },
    memberList: [
      {
        thumb: require('@/assets/img/data/color_nuest-01.jpg'),
        name: 'JR',
      },
      {
        thumb: require('@/assets/img/data/color_nuest-02.jpg'),
        name: 'ARON',
      },
      {
        thumb: require('@/assets/img/data/color_nuest-03.jpg'),
        name: 'BAEKHO',
      },
      {
        thumb: require('@/assets/img/data/color_nuest-04.jpg'),
        name: 'MINHYUN',
      },
      {
        thumb: require('@/assets/img/data/color_nuest-05.jpg'),
        name: 'REN',
      },
    ],
    lyrics:
      '시간이 갈수록 너를 원해 Baby 네가 있어야 할 곳은 여기니까 욕심이 또 자라 나의 꿈에 갇힌대도 내 선택은 같을 거야',
    lyricsDuration: 90,
    song: {
      title: 'BET BET',
      date: 'JUST ONE DAY',
    },
    albumList: [
      {
        thumb: require('@/assets/img/data/nu01.jpg'),
      },
      {
        thumb: require('@/assets/img/data/nu02.jpg'),
      },
      {
        thumb: require('@/assets/img/data/nu03.jpg'),
      },
    ],
  },
]

export default artistData
