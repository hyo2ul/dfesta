const goodsDatas = [
  {
    title: 'D’FESTA SEOUL <br class="is-word-mobile" /> OFFICIAL BOOK',
    text: `‘THE EXHIBITION &lt;10년의 기록, 그리고 새로운 이야기&gt;’의 화보가 그대로 담긴 전시 도록. <br class="is-desktop"> 각 그룹별로 구성된 전시 도록을 통해 D’FESTA의 감동을 그대로 간직할 수 있습니다.`,
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_01_01.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_02.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_03.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_04.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_05.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_06.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_07.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_08.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_09.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_10.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_01_11.jpg'),
      },
    ],
  },
  {
    title: 'D’FESTA POST CARDS',
    text: '‘THE EXHIBITION <10년의 기록, 그리고 새로운 이야기>’의 화보를 그대로 담은 POST CARDS. <br class="is-desktop"> 총 216 종류의 화보를 한 손에 잡히는 POST CARDS로 만나볼 수 있습니다.',
    url: '#none',
    goodsGroupData: [
      {
        thumb: require('@/assets/img/img_goods_figure_02_00.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_01.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_02.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_03.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_04.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_05.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_06.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_07.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_08.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_09.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_10.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_11.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_12.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_02_13.jpg'),
      },
    ],
  },
  {
    title: 'FOLDABLE ECO BAG <br> WITH PAINTINGS BY <br class="is-word-mobile" /> THE ARTISTS',
    text: 'D’FESTA를 축하하는 아티스트의 마음과 손길이 담긴 ’ARTIST PAINTING’으로 만든 에코백. <br class="is-desktop"> 그룹별 3가지 타입으로, 총 27개의 색다른 에코백을 박스에 담아 선사합니다.',
    url: '#none',
    goodsGroupData: [
      {
        thumb: require('@/assets/img/img_goods_figure_03_00_new.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_03_01_new.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_03_02_new.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_03_03_new.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_03_04_new.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_03_05_new.jpg'),
      },
    ],
  },
  {
    title: 'D’FESTA MASKING TAPE',
    text: '’ARTIST PAINTING’과 ‘XR STAGE’ 두가지 버전으로 선보이는 MASKING TAPE. <br class="is-desktop"> 일상에서 활용도 높은 마스킹 테이프가 디페스타의 기억을 오래도록 기억할 수 있게 합니다.',
    url: '#none',
    goodsVerticalData: [
      {
        thumb: require('@/assets/img/img_goods_figure_04_01_new.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_04_02_new.jpg'),
      },
    ],
  },
  {
    title: 'D’FESTA L - Folder',
    text: `‘THE EXHIBITION <10년의 기록, 그리고 새로운 이야기>’의 화보를 담은 L - FOLDER. <br class="is-desktop"> D’FESTA에 참여한 K-POP 아티스트 9그룹, 69명의 화보를 L - FOLDER에 담았습니다.`,
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_06_01.jpg'),
      },
    ],
  },
  {
    title: `5 COLOR BALLPOINT PENCIL SET <br class="is-word-mobile" /> with Tin Case `,
    text: `’ARTIST PAINTING’으로 만든 5 COLOR BALLPOINT PENCIL SET. <br class="is-desktop"> 휴대가 간편한 틴케이스에 담긴 5가지 색의 볼펜은 K-POP 아티스트와의 커플템으로 좋습니다.`,
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_07_01.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_07_02.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_07_03.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_07_04.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_07_05.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_07_06.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_07_07.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_07_08.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_07_09.jpg'),
      },
    ],
  },
  {
    title: `THE ARTIST PIC`,
    text: '69명의 K-POP 아티스트와의 D’FESTA의 추억을 남길 수 있는 THE ARTIST PIC. <br class="is-desktop"> 특별한 추억을 한 장의 사진으로 영원히 간직하세요!',
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_08_01.jpg'),
      },
    ],
  },
  {
    title: 'D’FESTA ARTIST PAINTING <br class="is-word-mobile" /> T-SHIRT',
    text: `각 그룹의 개성이 담긴 ’ARTIST PAINTING’으로 만든 T-SHIRT. <br> S, M 두 가지 사이즈로 만나보는 ARTIST PAINTING T-SHIRT로 일상에서 디페스타를 즐길 수 있습니다.`,
    url: '#none',
    goodsVerticalData: [
      {
        thumb: require('@/assets/img/img_goods_figure_09_01_new.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_09_02_new.jpg'),
      },
    ],
  },
  {
    title: `D’FESTA POSTER`,
    text: `디페스타를 빛낸 K-POP 아티스트 9그룹의 단체 화보를 담은 POSTER. <br class="is-desktop"> 하나의 작품이 된 그룹 화보를 2가지 타입으로 만나볼 수 있습니다.`,
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_10_1.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_2.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_3.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_4.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_5.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_6.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_7.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_8.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_9.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_10.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_10_11.jpg'),
      },
    ],
  },
  {
    title: `D’FESTA ARTIST PAINTING CARD`,
    text: `아티스트가 직접 페인팅한 작품을 고스란히 담아낸 카드와 봉투 세트입니다. <br class="is-desktop"> 9 그룹 각각의 특색이 묻어난 특별한 작품을 미니멀한 사이즈로 소장할 수 있습니다.`,
    url: '#none',
    goodsVerticalData: [
      {
        thumb: require('@/assets/img/img_goods_figure_11_01_new.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_11_02_new.jpg'),
      },
    ],
  },
  {
    title: `THE MESSAGE - Color <br class="is-word-mobile" /> Portraits With 69 Artist’s`,
    text: `69명 아티스트들의 새로운 개인 화보로 완성된 “THE MESSAGE”에는 D’FESTA를 직접 방문한 아티스트들의 특별한 순간이 담겨 있습니다. <br class="is-desktop"> 아티스트가 또 다른 아티스트들의 화보에 남겨둔 메세지를 함께 기억해 보세요.`,
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_12_01.jpg'),
      },
    ],
  },
  {
    title: `THE CALENDAR`,
    text: `디페스타 컨셉화보로 완성된 그룹별 탁상용 캘린더입니다. 월별로 새로워지는 아티스트의 <br class="is-desktop"> 화보와 일상을 함께해 보세요. 앞면은 컬러 화보로, 뒷면은 흑백 화보로 구성되어 있어 더욱 다채롭습니다.`,
    url: '#none',
    goodsGridData: [
      {
        thumb: require('@/assets/img/img_goods_figure_13_01.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_13_02.jpg'),
        isColWide: true,
      },
      {
        thumb: require('@/assets/img/img_goods_figure_13_03.jpg'),
        isCol: true,
      },
      {
        thumb: require('@/assets/img/img_goods_figure_13_04.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_13_05.jpg'),
        isCol: true,
      },
      {
        thumb: require('@/assets/img/img_goods_figure_13_06.jpg'),
        isColWide: true,
      },
    ],
  },
  {
    title: `SPECIAL KIT`,
    text: `TICKET CARD WITH NECK STRAP CARD HOLDER / TATTOO STICKER / JELLY STICKER`,
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_20_01.jpg'),
      },
    ],
  },
  {
    title: `SPECIAL <br class="is-word-mobile" /> GIFT`,
    text: `디페스타에 입장하는 모든 분들께 제공되는 <br class="is-word-mobile" /> 스페셜 기프트입니다. `,
    url: '#none',
    goodsCover: {
      thumb: require('@/assets/img/img_goods_figure_14_01.jpg'),
      thumbMo: require('@/assets/img/img_goods_figure_14_01_mo.jpg'),
    },
  },
  {
    title: `TICKET CARD with <br class="is-word-mobile" /> Neck Strap Card Holder`,
    text: `K-POP 아티스트 9그룹, 69명의 아름다운 얼굴로 완성된 TICKET CARD. <br class="is-desktop"> 글로벌 K-POP 페스티벌 D’FESTA의 특별함을 담은 TICKET CARD를 전용 홀더에 담아 간직하세요!`,
    url: '#none',
    cardHolder: [
      {
        thumb: require('@/assets/img/img_goods_figure_15_01.jpg'),
        caption: `특별히 하네다 공항에서 펼쳐지는 D’FESTA TOKYO II 에서는 <br class="is-desktop"> 새로운 사진으로 선보입니다.`,
      },
      {
        thumb: require('@/assets/img/img_goods_figure_15_02.jpg'),
        captionMo: `K-POP 아티스트 9그룹, 69명의 아름다운 얼굴로 완성된 TICKET CARD. <br class="is-desktop"> 글로벌 K-POP 페스티벌 D’FESTA의 특별함을 담은 TICKET CARD를 전용 홀더에 담아 간직하세요!`,
      },
    ],
  },
  {
    title: `TATTOO STICKER / <br class="is-word-mobile" /> JELLY STICKER `,
    text: `각 그룹별 로고와 ’ARTIST PAINTING’를 합쳐 만든 타투 스티커와 젤리 스티커를 스페셜 기프트로 제공합니다. <br class="is-desktop"> D’FESTA의 행복한 기억을 간직해보세요.`,
    url: '#none',
    goodsGridData: [
      {
        thumb: require('@/assets/img/img_goods_figure_16_01.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_16_02.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_16_03.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_16_04.jpg'),
      },
    ],
  },
  {
    title: `D’FESTA OFFICIAL POSTER`,
    text: `K-POP 아티스트 9그룹, 69명의 흑백 화보가 모두 담긴 POSTER. <br class="is-desktop"> 글로벌 K-POP 페스티벌 D’FESTA를 한 장의 POSTER에 담았습니다.`,
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_17_01.jpg'),
      },
    ],
  },
  {
    title: `THE GACHA`,
    text: `D’FESTA TOKYO II 에서는 그룹 로고와 아티스트 얼굴, 그룹별 단체 화보로 표현된 다양한 핀뱃지가 준비되어 있습니다. <br class="is-desktop"> 특별 이벤트로 제공되는 기프트로 ‘가차’를 통해 랜덤으로 제공됩니다.`,
    url: '#none',
    goodsSwiperData: [
      {
        thumb: require('@/assets/img/img_goods_figure_18_01.jpg'),
      },
    ],
  },
  {
    title: `SPECIAL ECO BAG`,
    text: `D’FESTA TOKYO II 에서는 일정 이상의 굿즈를 구매하신 분들께 무료로 담아 드리는 <br class="is-desktop"> 세 가지 종류의  ‘D’FESTA’ 로고가 쓰여진 에코백이 제공됩니다.`,
    url: '#none',
    goodsVerticalData: [
      {
        thumb: require('@/assets/img/img_goods_figure_19_01.jpg'),
      },
      {
        thumb: require('@/assets/img/img_goods_figure_19_02.jpg'),
      },
    ],
  },
  // {
  //   title: ``,
  //   text: ``,
  //   url: '#none',
  //   goodsSwiperData: [
  //     {
  //       thumb: require('@/assets/img/img_goods_figure_11_01.jpg'),
  //     },
  //   ],
  // },
]

export default goodsDatas
